# SimpleDashboardCards

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.6.

# Note

Basic installation and how to run the app.

## Installation

Clone this repository in your local machine.
Open terminal and go to the app path, and use the Node Package Manager to install dependencies.

```bash
npm install
```

## Run
Run this command to start the app.

```bash
ng serve
```
