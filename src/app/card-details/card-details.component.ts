import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  @Input() data = '';
  @Output() emitClose = new EventEmitter<string>();
  faTimes = faTimes
  dataList

  constructor() { }

  ngOnInit(): void {
    this.dataList = this.data
  }

  close(param) {
    this.emitClose.emit(param);
  }

}
