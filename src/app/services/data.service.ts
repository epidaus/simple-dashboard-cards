import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class DataService {

  API_URL: string = 'https://staging.feets.me/api/v3/dashboard/learner_cards?start_at=2020-12-01&end_at=2020-12-31'

  constructor(private httpclient: HttpClient) { }

  getData(): Observable<any> {
    let header = new HttpHeaders({
      "X-Learner-Email": "user1@feets.me",
      "X-Learner-Token": "Wkir6KkJ1SBy-mwn4_PX"
    });
    return this.httpclient.get(`${this.API_URL}`, {headers: header})
  }
}
