import { Component, OnInit } from '@angular/core';
import { faEllipsisH, faPencilAlt, faUserAlt } from '@fortawesome/free-solid-svg-icons';

import { DataService } from './services/data.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  faEllipsisH = faEllipsisH
  faPencilAlt = faPencilAlt
  faUserAlt = faUserAlt
  dataArr

  productSales: any[]
  productSalesMulti: any[]

  view: any[] = null;

  // options
  showLegend: boolean = true;
  showLabels: boolean = true;

  gradient: boolean = false;
  isDoughnut: boolean = true;

  legendPosition: string = 'below';

  colorSchemePositive = {
    domain: ['#7BE160']
  };

  colorSchemeNegative = {
    domain: ['#F55858']
  }
  
  findStress
  findHappiness
  findEngagement
  chartDataStress:any[]
  chartDataHappiness:any[]
  chartDataEngagement:any[]

  detailsPage = false
  details
  detailData = ""
  
  constructor(private data: DataService) {}
  ngOnInit(): void {
    this.data.getData().subscribe(res => {
      this.dataArr = res
      
      this.findStress = this.dataArr.find(el => el.attribute.title == 'stress_index')
      this.findHappiness = this.dataArr.find(el => el.attribute.title == 'happiness_index')
      this.findEngagement = this.dataArr.find(el => el.attribute.title == 'engagement_index')
      this.chartDataStress = [
        {
          name: "Positive",
          value: this.findStress.attribute.card_meta.body_chart_users_positive
        },
        {
          name: "Negative",
          value: this.findStress.attribute.card_meta.body_chart_users_negative
        }
      ]
      this.chartDataHappiness = [
        {
          name: "Positive",
          value: this.findHappiness.attribute.card_meta.body_chart_users_positive
        },
        {
          name: "Negative",
          value: this.findHappiness.attribute.card_meta.body_chart_users_negative
        }
      ]
      this.chartDataEngagement = [
        {
          name: "Positive",
          value: this.findEngagement.attribute.card_meta.body_chart_users_positive
        },
        {
          name: "Negative",
          value: this.findEngagement.attribute.card_meta.body_chart_users_negative
        }
      ]

    })
    
  }

  showDetails(param, id) {
    this.detailsPage = param
    this.detailData = this.dataArr.find(x => x.id == id)
  }

  closeDetails(param) {
    this.detailsPage = param;
  }

  hideCard() {

  }

}
